module.exports = {
  activeBrowsers: [{
    url: "http://localhost:4444/wd/hub",
    browserName: "chrome"
  }],
  plugins: {
    'xunit-reporter': {
      'output': './test-results/testfile.xml'
    },
    istanbub: {
      dir: "./coverage",
      reporters: ["text-summary", "lcov"],
      include: [
        "**/*.html"
      ],
      exclude: [
        "**/test/**"
      ],
      thresholds: {
        global: {
          statements: 90,
          branches: 90,
          functions: 90,
          lines: 90
        }
      }
    }
  }
};
